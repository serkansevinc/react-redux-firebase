import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCyy-EuI0nMVFiUk4X-AxEsU_jFqyO_8-s",
  authDomain: "serkan-mycompany.firebaseapp.com",
  databaseURL: "https://serkan-mycompany.firebaseio.com",
  projectId: "serkan-mycompany",
  storageBucket: "serkan-mycompany.appspot.com",
  messagingSenderId: "399685023879"
};
firebase.initializeApp(config);

export default firebase;